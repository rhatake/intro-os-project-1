/*
 * RandomNumbers.h
 *
 *  Created on: Oct 8, 2014
 *      Author: Jayme Mann
 */

#ifndef RANDOMNUMBERS_H_
#define RANDOMNUMBERS_H_

int pick_number_between(int max, int min);

#endif /* RANDOMNUMBERS_H_ */
