/*
 * Game.h
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#ifndef GAME_H_
#define GAME_H_

#include "Options.h"

void start_game(struct Options* options);

#endif /* GAME_H_ */
