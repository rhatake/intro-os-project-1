/*
 * IOHelper.h
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#ifndef IOHELPER_H_
#define IOHELPER_H_

#include "IOSystem.h"
#include "GameData.h"

#define MENU "1) Play\n2) Quit\n"

#define PLAY 1
#define QUIT 2

#define INPUT_READ_SIZE 25

typedef char Input;

void display_menu();
int get_menu_option();

Input* get_player_input();
int wants_to_pass(Input* input);
int get_number_entered(Input* input);
void destroy_input(Input* input);

int get_player_number(GameData* game);



#endif /* IOHELPER_H_ */
