/*
 * GameData.c
 *
 *  Created on: Oct 8, 2014
 *      Author: Jayme Mann
 */

#include "GameData.h"
#include "RandomNumbers.h"

#include <stdlib.h>

static void initialize_player_list(GameData* game, Player* players, int number_of_players);
static void initialize_player(GameData* game, Player* player, int id);

void initialize(GameData* game, struct Options* options)
{
	game->options = options;
	game->players = (Player*) malloc(sizeof(Player)*options->number_of_players);
	initialize_player_list(game, game->players, options->number_of_players);

}

static void initialize_player_list(GameData* game, Player* players, int number_of_players)
{
	int i;
	for (i = 0; i < number_of_players; i++) {
		initialize_player(game, players+i, i+1);
	}
}

static void initialize_player(GameData* game, Player* player, int id)
{
	reset(player);
	set_id(player, id);
	player->options = game->options;
}

void uninitialize(GameData* game)
{
	free(game->players);
}

int is_valid_player(GameData* game, int player_id)
{
	return !(player_id > game->options->number_of_players) | (player_id < 1);
}

int is_players_turn(GameData* game, int player_number)
{
	return game->turn_number == player_number;
}

void next_turn(GameData* game)
{
	// change who's turn it is
	game->turn_number++;

	if (game->turn_number > game->options->number_of_players) {
		game->turn_number = 1;
	}
}

// Returns:
//	0 guess is correct
//	-1 guess is too low
//	1 guess is too high
int check_answer(GameData* game, int guess)
{
	if (guess == game->random_number) {
		return 0;
	}
	else if (guess < game->random_number) {
		return -1;
	}
	else {
		return 1;
	}
}

Player* get_player_by_id(GameData* game, int id)
{
	return &game->players[id - 1];
}

void reset_game_data(GameData* game)
{
	game->random_number = pick_number_between(game->options->minimum_number, game->options->maximum_number);
	game->turn_number = pick_number_between(1, game->options->number_of_players);
	game->has_won = 0;

	// reset the player portion of the gamedata by simply reinitializing it
	initialize_player_list(game, game->players, game->options->number_of_players);
}


