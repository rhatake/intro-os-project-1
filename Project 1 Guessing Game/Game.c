/*
 * Game.c
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#include "Game.h"
#include "IOHelper.h"

#include <stdlib.h>

// private functions
static void start(GameData* game);
static void run();
static void take_turn(GameData* game, int player);
static void tell_player_how_many_more_times_he_can_pass(GameData* game, Player* player);
static void tell_player_he_cant_pass(GameData* game, Player* player);
static void process_result(GameData* game, int result);

void start_game(struct Options* options)
{
	GameData* game_data = (GameData*) malloc(sizeof(GameData));

	initialize(game_data, options);
	start(game_data);
	uninitialize(game_data);

	free(game_data);

}

static void start(GameData* game)
{
	while (1) {
		display_menu();

		int response = get_menu_option();

		if (response == PLAY) {
			reset_game_data(game);
			run(game);
		} else {
			// If the response wasn't to play, then it was to quit
			return;
		}
	}
}

static void run(GameData* game)
{
	int player;

	while (!game->has_won) {
		// prompt for the player's number at the terminal
		player = get_player_number(game);

		if (is_players_turn(game, player)) {
			take_turn(game, player);
		} else {
			write("You must wait your turn");
		}
	}

	writef("Player %d has won the game\n", player);
}


// This is the core of what was outlined in the psuedo code
static void take_turn(GameData* game, int player)
{
	Player* player_structure = get_player_by_id(game, player);
	Input* input;
	int change_turn = 1;

	write("Enter your guess: ");
	input = get_player_input();

	if (wants_to_pass(input)) {
		if (can_pass(player_structure)) {
			player_chose_to_pass(player_structure); // record that the player passed this round in its structure
			tell_player_how_many_more_times_he_can_pass(game, player_structure);
		} else {
			tell_player_he_cant_pass(game, player_structure);
			change_turn = 0;
		}
	} else {
		player_chose_to_play(player_structure); // record that the player didn't passed this round in its structure
		int guess = get_number_entered(input);
		int result = check_answer(game, guess); // consult the game structure on whether the correct number was entered
		process_result(game, result);
	}

	if (change_turn) {
		next_turn(game);
	}
}

static void tell_player_how_many_more_times_he_can_pass(GameData* game, Player* player)
{
	writef("You have passed %d time(s), you have %d more times left",
			player->passes_this_round,
			game->options->number_of_passes_per_player_per_game - player->passes_this_round);
}

static void tell_player_he_cant_pass(GameData* game, Player* player)
{
	if (player->passes_this_round
			>= game->options->number_of_passes_per_player_per_game) {
		write("You may not pass anymore this round");
	} else {
		write("You must take your turn this time");
	}
}

static void process_result(GameData* game, int result)
{
	if (result == 0) {
		write("You Won!\n");
		game->has_won = 1;
	} else if (result == -1) {
		write("Too low");
	} else {
		write("Too high");
	}
}
