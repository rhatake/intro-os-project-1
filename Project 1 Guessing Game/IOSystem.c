/*
 * IOSystem.c
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>

#include "IOSystem.h"

#define TRUE 1
#define FALSE 0

#define SIZE 256

// private functions
static int input_type_int(char* in_buffer);
static int read_data(char* in_buffer, int size);

int read_int(int* dest)
{
	char temporary_buffer[SIZE];

	read_data(temporary_buffer, SIZE);

	if (input_type_int(temporary_buffer) == TRUE) {
		*dest = atoi(temporary_buffer);
	}
	else {
		return -1;
	}

	return 0;
}

static int input_type_int(char* in_buffer)
{
	char* character_pointer;
	for (character_pointer = in_buffer; *character_pointer != '\0'; character_pointer++) {
		if (isalpha(*character_pointer)) {
			return FALSE;
		}
	}

	return TRUE;
}


int read_string(char* in_buffer, int size)
{
	char* temporary_buffer = (char*) malloc(size);

	read_data(temporary_buffer, size);

	strncpy(in_buffer, temporary_buffer, size);

	free(temporary_buffer);

	return 0;
}

static int read_data(char* in_buffer, int size)
{
	// fgets() reads in at most one less than size characters
	void* return_value = fgets(in_buffer, size + 1, stdin);

	if (return_value == NULL) {
		return -1;
	}
	else {
		return 0;
	}
}

void write(const char* message)
{
	printf("%s", message);
}

void writef(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	vprintf(format, args);
}
