/*
 * Options.h
 *
 *  Created on: Oct 8, 2014
 *      Author: Jayme Mann
 */

#ifndef OPTIONS_H_
#define OPTIONS_H_

struct Options {
	int number_of_players;
	int number_of_passes_per_player_per_game;
	int number_of_allowed_consecutive_passes_per_player;
	int minimum_number;
	int maximum_number;
};


#endif /* OPTIONS_H_ */
