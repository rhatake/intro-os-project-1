/*
 * GameData.h
 *
 *  Created on: Oct 8, 2014
 *      Author: Jayme Mann
 */

#ifndef GAMEDATA_H_
#define GAMEDATA_H_

#include "Player.h"

typedef struct
{
	int turn_number;
	int random_number;
	int has_won;
	Player* players;
	struct Options* options;
} GameData;

void initialize(GameData* game, struct Options* options);
void uninitialize(GameData* game);

int is_valid_player(GameData* game, int player);
int is_players_turn(GameData* game, int player_number);
int check_answer(GameData* game, int guess);
void next_turn(GameData* game);

Player* get_player_by_id(GameData* game, int id);
void reset_game_data(GameData* game);

#endif /* GAMEDATA_H_ */
