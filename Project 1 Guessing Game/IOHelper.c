/*
 * IOHelper.c
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */


#include "IOHelper.h"
#include <stdlib.h>
#include <string.h>

void display_menu()
{
	write(MENU);
}

int get_menu_option()
{
	int choice = -1;

	while (choice != PLAY && choice != QUIT) {
		do {
			write("#: ");
		} while (read_int(&choice) == -1);
	}

	return choice;
}

Input* get_player_input()
{
	Input* input = (char*) malloc(INPUT_READ_SIZE);

	read_string(input, INPUT_READ_SIZE);

	return input;
}

int wants_to_pass(Input* input)
{
	if (strncmp(input, "PASS", 4) == 0) {
		return 1;
	}
	else {
		return 0;
	}
}

void destroy_input(Input* input)
{
	free(input);
}

int get_number_entered(Input* input)
{
	return atoi(input);
}

int get_player_number(GameData* game)
{
	int player_id = -1;

	while (1) {
		do {
			write("\nPlayer Number: ");
		} while (read_int(&player_id) == -1);

		if (!is_valid_player(game, player_id)) {
			write("Invalid Player");
		}
		else {
			break;
		}
	}

	return player_id;
}

