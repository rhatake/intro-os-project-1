/*
 * IOSystem.h
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#ifndef IOSYSTEM_H_
#define IOSYSTEM_H_

#include <stdio.h>

int read_string(char* in_buffer, int size);
int read_int(int* dest);

void write(const char* message);

void writef(const char* format, ...);


#endif /* IOSYSTEM_H_ */
