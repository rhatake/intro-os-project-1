/*
 * Player.h
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Options.h"

typedef struct
{
	int id;
	int passes_this_round;
	int consecutive_passes_taken;
	struct Options* options;
} Player;

void set_id(Player* player, int id);

// resets everything but the player id
void reset(Player* player);

int can_pass(Player* player);

void player_chose_to_pass(Player* player);

void player_chose_to_play(Player* player);

#endif /* PLAYER_H_ */
