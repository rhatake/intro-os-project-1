/*
 * Player.c
 *
 *  Created on: Oct 6, 2014
 *      Author: Jayme Mann
 */

#include "Player.h"


void set_id(Player* player, int id)
{
	player->id = id;
}

// resets everything but the player id
void reset(Player* player)
{
	player->consecutive_passes_taken = 0;
	player->passes_this_round = 0;
}

int can_pass(Player* player)
{
	if (player->consecutive_passes_taken < player->options->number_of_allowed_consecutive_passes_per_player
			&& player->passes_this_round < player->options->number_of_passes_per_player_per_game) {
		return 1;
	}
	else {
		return 0;
	}
}

void player_chose_to_pass(Player* player)
{
	player->consecutive_passes_taken++;
	player->passes_this_round++;
}

void player_chose_to_play(Player* player)
{
	player->consecutive_passes_taken = 0;
}
