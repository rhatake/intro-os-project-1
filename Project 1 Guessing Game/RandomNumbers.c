/*
 * RandomNumbers.c
 *
 *  Created on: Oct 8, 2014
 *      Author: Jayme Mann
 */

#include "RandomNumbers.h"

#include <stdlib.h>
#include <math.h>

// Note: this is a linux specific header file
#include <time.h>

static double fractional_part(double number);
static double saw_tooth_wave(double offset, double minimum, double maximum);

// http://mathworld.wolfram.com/SawtoothWave.html
// Use a sawtooth wave to restrict random numbers
// My modification of the equation:
//  S(x) = (range * fractional_part(offset / range)) + minimum

int pick_number_between(int min, int max)
{
	srandom(time(NULL));
	return saw_tooth_wave(random(), min, max);
}

static double saw_tooth_wave(double offset, double minimum, double maximum)
{
	double range = labs(minimum - maximum) + 1;

	float fraction = fractional_part(offset / range);

	return (range * fraction) + minimum;
}

static double fractional_part(double number)
{
	return number - floor(number);
}
