#include <stdio.h>

#include "Game.h"

// Initialize and start game
int main()
{
	struct Options options = {
			.number_of_players = 2,
			.number_of_passes_per_player_per_game = 3,
			.number_of_allowed_consecutive_passes_per_player = 1,
			.minimum_number = 1,
			.maximum_number = 10,
	};

	start_game(&options);

	return 0;
}
