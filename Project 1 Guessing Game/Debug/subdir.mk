################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Game.c \
../GameData.c \
../IOHelper.c \
../IOSystem.c \
../Player.c \
../RandomNumbers.c \
../main.c 

OBJS += \
./Game.o \
./GameData.o \
./IOHelper.o \
./IOSystem.o \
./Player.o \
./RandomNumbers.o \
./main.o 

C_DEPS += \
./Game.d \
./GameData.d \
./IOHelper.d \
./IOSystem.d \
./Player.d \
./RandomNumbers.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Solaris C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


